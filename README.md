# Welcome to our workshop repository! 

Here, you will find the teaching materials for the "Next-generation sequencing association studies" workshop given at the [5th Sardinian International Summer School]("http://www.irgb.cnr.it/index.php?option=com_k2&view=item&layout=item&id=42&Itemid=298&lang=en"), June 20-24, 2016.


[Head directly to the workshop course](http://bit.ly/sardinian_workshop)!


## Authors
* Arthur Gilly (ag15@sanger.ac.uk)
* Sophie Hackinger (sh29@sanger.ac.uk)
* Eleftheria Zeggini (eleftheria@sanger.ac.uk)